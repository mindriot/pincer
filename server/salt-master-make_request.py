import datetime
import subprocess


import salt.client
import salt.output

local = salt.client.LocalClient()


url = 'https://isitchristmas.com'


def GenerateWgetCommand(    url, 
                            prefix, 
                            user_agent="Mozilla/5.0 (Windows NT 5.1; rv:13.0) Gecko/20100101 Firefox/13.0.1",
                            recursion=1):
    """
    WGET PARAMS
    ===========
    Static:
    * -t 5                      - Try 5 times
    * -T 5                      - Timeout after 5s
    * --no-dns-cache            - Disable DNS caching
    * -4                        - We are only looking for IPV4 traffic manipulation
    * --no-cache                - Disable server-side caching
    * --save-headers            - Store the headers in the file (delimited by CR)
    * --no-check-certificate    - Disable cert verification

    Dynamic:
    * -P <prefix>               - Directory prefix is datetime in tmp
    * -U <user_agent>           - User-Agent String
    * -r -l <level> -p          - Level of recursion (should be 1)
    """

    output = "wget"
    
    #static
    output+= " -t 5"
    output+= " -T 5"
    output+= " --no-dns-cache"
    output+= " -4"
    output+= " --no-cache"
    output+= " --save-headers"
    output+= " --no-check-certificate"
    
    #dynamic
    output+= " -P /tmp/%s"  % prefix
    output+= " -U \"%s\""   % user_agent
    output+= " -r -l %d -p" % recursion
    
    #url
    output+= " %s"          % url

    return output





def PerformRequests(url):
    prefix = datetime.datetime.now().strftime('%d%b%Y_%H%M').upper()
    wget_cmd = GenerateWgetCommand(url, prefix)

    print '[+] Performing Request Across All Minions'
    results = local.cmd('*', 'pincer.request', arg=[wget_cmd, prefix])

    print '[+] Downloading All Files From All Minions'
    results = local.cmd('*', 'cp.push', arg=['/tmp/%s.tgz' % prefix])
    print results




def RefreshAllMinions():
    print '[+] Approving all minions that are standing by'
    subprocess.call("/usr/bin/salt-key -A '*'", shell=True)

    print '[+] Ensuring our pincer modules is pushed to all of them'
    subprocess.call("/usr/bin/salt '*' saltutil.sync_modules", shell=True)

    #test the pipes
    print '[+] Pinging All Minions'
    print local.cmd('*', 'test.ping')



def main():
    RefreshAllMinions()
    PerformRequests(url)



if __name__ == '__main__':
    main()