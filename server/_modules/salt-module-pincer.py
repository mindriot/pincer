import json
import logging
import subprocess

log = logging.getLogger(__name__)

log.info('hello from pincer module')


def request(wget_cmd, prefix):
    log.info('WGET CMD: %s' % wget_cmd)
    #run wget
    subprocess.check_output(wget_cmd, shell=True)
    #bundle the output
    subprocess.check_output('tar -zcvf /tmp/%s.tgz -C /tmp/%s .' % (prefix, prefix), shell=True)
    #delete the old stuff
    subprocess.check_output('rm -rf /tmp/%s/' % prefix, shell=True)
    return wget_cmd

"""
def __virtual__():
    return 'pincer'
"""