
#Test web server
from twisted.internet.protocol import ServerFactory
from twisted.internet import reactor
from twisted.web.server import Site
from twisted.web.resource import Resource
import json
import geoip
import logging
import logging.handlers
import os

LOGGING_LEVEL = logging.DEBUG
LOG_DIR = "../log"
PATH_TO_CONFIG = "../config/config.json"


def readConfig(pathToConfig):
    with open(pathToConfig, "rb") as f:
        return json.loads(f.read())

def isIPOnBadList(ipaddress):
    conf = readConfig(PATH_TO_CONFIG)
    country = geoip.country(ipaddress)
    print "Request from country:" + country
    if country in conf["bad_country"]:
        return True
    return False

def _initLogging():
    #set up the basic logger
    logger = logging.getLogger()
    logger.handlers = []
    logger.setLevel(LOGGING_LEVEL)
    formatter = logging.Formatter('[%(asctime)-23s] %(thread)d:%(threadName)-8s %(levelname)-7s  %(module)s->%(funcName)s(%(lineno)s): %(message)s')

    #set up the console logger
    sh = logging.StreamHandler()
    sh.setFormatter(formatter)
    sh.setLevel(LOGGING_LEVEL)
    logger.addHandler(sh)

    #set up the file logger
    if(os.path.isdir(LOG_DIR) == False):
        os.makedirs(LOG_DIR)
    trf = logging.handlers.TimedRotatingFileHandler(LOG_DIR + '/log.log', when='midnight', interval=1, backupCount=5)
    trf.setFormatter(formatter)
    trf.setLevel(LOGGING_LEVEL)
    logger.addHandler(trf)

    logging.info("Logging online")

#determine if a malicious payload shoude be sent
def serverPayload(requestingIP):
    #load config to check hosts

        if isIPOnBadList(requestingIP):
            print "Host is being served bad things"
            return "<html><body>Bad things</body></html>"
        else:
            print "Host is not selected for malicious payload"
            return "<html><body>Good things</body></html>"

class PincerServer(Resource):
    isLeaf = True

    def render_GET(self, request):
        host = request.getHost()
        print "Request received: " + host.host

        return serverPayload(host.host)

_initLogging()
resource = PincerServer()
factory = Site(resource)
reactor.listenTCP(8888, factory)
reactor.run()