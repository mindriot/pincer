#need chrome driver
#https://sites.google.com/a/chromium.org/chromedriver/downloads
#http://phantomjs.org/download.html

from selenium import webdriver
import logging
import logging.handlers
import os
import json

LOGGING_LEVEL = logging.DEBUG
LOG_DIR = "../log"
PATH_TO_CONFIG = "../config/client_config.json"

phantom_exe = r"../res/phantomjs-2.0.0-windows/bin/phantomjs.exe"

def readConfig(pathToConfig):
    with open(pathToConfig, "rb") as f:
        return json.loads(f.read())

def getNextJobFromQueue():
    logging.debug("Getting task from queue")

def makeRequest(url):
    logging.info("Making request for: " + url)

    driver = webdriver.PhantomJS(executable_path=phantom_exe)
    driver.get(url)
    res = {}
    res["page_source"] = driver.page_source



def _initLogging():
    #set up the basic logger
    logger = logging.getLogger()
    logger.handlers = []
    logger.setLevel(LOGGING_LEVEL)
    formatter = logging.Formatter('[%(asctime)-23s] %(thread)d:%(threadName)-8s %(levelname)-7s  %(module)s->%(funcName)s(%(lineno)s): %(message)s')

    #set up the console logger
    sh = logging.StreamHandler()
    sh.setFormatter(formatter)
    sh.setLevel(LOGGING_LEVEL)
    logger.addHandler(sh)

    #set up the file logger
    if(os.path.isdir(LOG_DIR) == False):
        os.makedirs(LOG_DIR)
    trf = logging.handlers.TimedRotatingFileHandler(LOG_DIR + '/log.log', when='midnight', interval=1, backupCount=5)
    trf.setFormatter(formatter)
    trf.setLevel(LOGGING_LEVEL)
    logger.addHandler(trf)

    logging.info("Logging online")

def clientLoop():
    pass

def main():
    _initLogging()
    logging.info("Starting")
    tests()

def tests():
    driver = webdriver.PhantomJS(executable_path=phantom_exe)
    logging.info("making request")
    driver.get("http://testserver.pincer.org:8888")
    print driver.get_screenshot_as_base64()
    logging.debug(driver.page_source)



if __name__ == "__main__":
    main()
    print "done"